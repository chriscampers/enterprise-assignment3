﻿using Enterprise___Assignment3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Enterprise___Assignment3.Controllers
{
    public class UserAccountsController : Controller
    {
        private const string USER_SESSION_OBJ = "USER";
        public User User
        {
            get
            {
                User user = Session[USER_SESSION_OBJ] as User;
                if (user == null)
                {
                    user = new User();
                    Session[USER_SESSION_OBJ] = user;
                }
                return user;
            }
            
        }
        // GET: UserAccounts
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ViewResult Index(User user, string submit)
        {
            if (submit.Equals("User Login"))
            {
                if (user.Reverse())
                {
                    user.UserType = AuthorizationType.User;
                    Console.WriteLine("WORKS",user);
                    Session[USER_SESSION_OBJ] = user;
                    return View("UserView", user);
                }
                else{
                    ViewBag.FailedLogin = "Login Failed Username or Password Incorrect";
                    return View();
                }
            }
            else
            {
                if(user.UserName.Equals("Amanda") || user.UserName.Equals("Ray"))
                {
                    if (user.Reverse())
                    {
                        user.UserType = AuthorizationType.Manager;
                        Console.WriteLine("WORKS");
                        Session[USER_SESSION_OBJ] = user;
                        return View("UserView", user);
                    }
                }
                else
                {
                    ViewBag.FailedLogin = "Login Failed Username or Password Incorrect";
                    return View();
                }
            }
            return View();
        }
    }
}