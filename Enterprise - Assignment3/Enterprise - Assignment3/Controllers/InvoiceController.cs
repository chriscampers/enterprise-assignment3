﻿using Enterprise___Assignment3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Enterprise___Assignment3.Controllers
{
    public class InvoiceController : Controller
    {
        private const string INVOICE_SESSION_OBJ = "Invoices";
        private const string USER_SESSION_OBJ = "USER";

        public InvoiceStorage InvoiceStorage
        {
            get
            {
                InvoiceStorage invoices = Session[INVOICE_SESSION_OBJ] as InvoiceStorage;
                if (invoices == null)
                {
                    invoices = new InvoiceStorage();
                    Session[INVOICE_SESSION_OBJ] = invoices;
                }
                return invoices;
            }
        }
        

        public User User
        {
            get
            {
                User user = Session[USER_SESSION_OBJ] as User;
                if (user == null)
                {
                    user = new User();
                    Session[USER_SESSION_OBJ] = user;
                }
                return user;
            }
        }
        public ActionResult Delegater(string submit)
        {
            if (submit.Equals("Manage "))
            {
                return View("UserView", "UserAccounts",User);
            }
            else if (submit.Equals("Add Invoice"))
            {
                return View("AddInvoice");
            }
            else if (submit.Equals("Review Recievables"))
            {
                return View("ReviewRecievables", InvoiceStorage);
            }
            else if (submit.Equals("Manage Invoices"))
            {
                return View("ManageInvoices", InvoiceStorage);
            }
            return View();
        }
        // GET: Invoice
        public ActionResult AddInvoice(Invoice invoice,string submit)
        {
            if (submit.Equals("Back"))
            {
                return View("../UserAccounts/UserView", User);
            }
            else if (submit.Equals("Add Invoice") || submit.Equals("Save Invoice")) 
            {
                InvoiceStorage.AddInvoice(invoice);
            }
            return View();
        }
        public ActionResult ReviewRecievables()
        {
            return View(InvoiceStorage);
        }
        public ActionResult ManageInvoices()
        {
            return View(InvoiceStorage);
        }
        public ActionResult EditInvoice(Invoice invoice)
        {
            return View("AddInvoice",invoice);
        }
    }
}