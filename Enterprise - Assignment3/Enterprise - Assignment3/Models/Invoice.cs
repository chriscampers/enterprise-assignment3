﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Enterprise___Assignment3.Models
{
    public enum Currency
    {
        CAD,
        US,
        EUR
    }
    public class Invoice
    {
     
        public Invoice()
        {
            IsPaid = false;
        }
        [Required(ErrorMessage = "Please Enter Clients Name")]
        public string ClientName { get; set; }

        [Required(ErrorMessage = "Please Enter Clients Address")]
        public string ClientAddress { get; set; }

        [Required(ErrorMessage = "Please Enter Date of Shipment")]
        public DateTime DateofShipment { get; set; }

        [Required(ErrorMessage = "Please Enter Payments Due Date")]
        public DateTime PaymentDueDate { get; set; }

        [Required(ErrorMessage = "Please Enter your Product Name")]
        public string ProductName { get; set; }

        [Required(ErrorMessage = "Please Enter Product Quantity")]
        public int ProductQuantity { get; set; }

        [Required(ErrorMessage = "Please Enter Unit Price")]
        public double UnitPrice { get; set; }

        [Required(ErrorMessage = "Please Enter Currency Type")]
        public Currency Currency { get; set; }

        public int InvoiceNumber { get; set; }
        public double TotalPrice { get { return Math.Round(ProductQuantity * UnitPrice, 2); } }
        public bool IsPaid { get; set; }

    }
}