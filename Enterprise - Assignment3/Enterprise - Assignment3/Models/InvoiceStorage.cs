﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Enterprise___Assignment3.Models
{
    public class InvoiceStorage
    {
        private List<Invoice> _invoiceList;

        public IEnumerable<Invoice> InvoiceList
        {
            get { return _invoiceList; }
        }
        public InvoiceStorage()
        {
            _invoiceList = new List<Invoice>();
        }
        public void AddInvoice(Invoice invoice)
        {
            
            if (invoice.InvoiceNumber > 0)
            {
                int temp = 0;
                foreach (Invoice i in InvoiceList.ToList())
                {
                    if (invoice.InvoiceNumber == i.InvoiceNumber)
                    {
                        _invoiceList[temp] = invoice;
                    }
                    temp++;
                }

  
            }
            else
            {
                invoice.InvoiceNumber = _invoiceList.Count + 1;
                _invoiceList.Add(invoice);
            }

        }

    }
}