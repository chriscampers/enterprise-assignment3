﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Enterprise___Assignment3.Models
{
    public enum AuthorizationType
    {
        User,
        Manager
    }
    public class User
    {
        public User()
        {

        }
        public string Name { get; set; }

        [Required(ErrorMessage = "Please Enter your username")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Please Enter your password")]
        public string Password { get; set; }
        public AuthorizationType UserType { get; set; }

        public bool Reverse()
        {
            char[] charArray = UserName.ToCharArray();
            Array.Reverse(charArray);
            if(new string(charArray).Equals(Password))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}